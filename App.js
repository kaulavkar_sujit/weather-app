/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import 'react-native-gesture-handler';
import Container from 'scenes/container'
import { Provider } from 'react-redux';
import { store } from './App/store';

const App: () => React$Node = () => {

  return (
    <Provider store={store}>
        <Container />
    </Provider>
  )
};

console.disableYellowBox = true;

export default App;
