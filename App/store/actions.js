
export const weatherDataSuccess = payload => ({
  type: 'WEATHER_REPORT_SUCCESS',
  payload: payload
});
