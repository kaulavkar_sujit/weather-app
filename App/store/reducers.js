import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import weatherReducer from './weatherReducer';

export default combineReducers({
    login: loginReducer,
    weatherReport: weatherReducer,
});
