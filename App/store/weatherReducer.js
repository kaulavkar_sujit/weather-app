
const INITIAL_STATE = {
    
}

const weatherReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'WEATHER_REPORT_SUCCESS':      
      return {
        ...state,
        weatherData: action.payload
      };
    default:
      return state;
  }
};

export default weatherReducer;
