import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    View,
    TouchableOpacity,   
    Image,
    Text
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Header from 'molecules/Header';
import backarrow from '../../assets/Images/arrow.png';
import avatar from '../../assets/Images/avatar.png';

const options = {
    title: 'Select Avatar',
  };

export default class viewProfile extends Component {

    constructor() {
        super() 

        this.state = {
            avatarSource: avatar
        }
    }

    openGallary = () => {
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
            } else {
              const source = { uri: response.uri };                  
              this.setState({
                avatarSource: source,
              });
            }
          });
    }

    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <Header
                        leftIcon={backarrow}
                        right
                        onLeftPress={() => this.props.navigation.pop()} />               

                        <View style={styles.topContainer}>
                                <TouchableOpacity style={styles.avatarSourceBtn}  onPress={this.openGallary}>
                                    <Image style={styles.avatarSourceBtn} source={this.state.avatarSource}/>
                                </TouchableOpacity>

                                <Text style={styles.name}>leonardo dicaprio</Text>
                                <Text style={styles.profession}>Actor</Text>
                        </View>

                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    topContainer: {
        marginTop: 20,
        width: '100%',
        height: 200,
        alignItems:'center',
        justifyContent: 'center'
    },
    avatarSourceBtn: {
        width: 150,
        height: 150,
        borderRadius: 75,
        borderColor: 'gray',
        borderWidth: 1
    },
    name: {
        marginTop: 10,
        color: 'black',
        fontSize: 20,
        alignSelf: 'center',
        fontWeight: '500'
    },
    profession: {
        marginTop: 10,
        color: 'black',
        fontSize: 18,
        alignSelf: 'center',
        fontWeight: '300'
    }
})