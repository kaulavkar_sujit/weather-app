import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import addProfile from '../addProfile';
import viewProfile from '../viewProfile';
import weatherDashboard from '../weatherDashboard';

const Stack = createStackNavigator();

function Container() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      headerMode="none"
      >
        <Stack.Screen name="addProfile" component={addProfile} />
        <Stack.Screen name="viewProfile" component={viewProfile} />
        <Stack.Screen name="weatherDashboard" component={weatherDashboard} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Container;