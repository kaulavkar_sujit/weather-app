import React, { Component } from 'react';
import {
    SafeAreaView,
    Keyboard,
    View,
    StatusBar,
} from 'react-native';
import ThemedTextInput from 'atoms/ThemedTextInput';
import ThemedButton from 'atoms/ThemedButton';
import Toast, {DURATION} from 'react-native-easy-toast';

export default class addProfile extends Component {

    constructor() {
        super()

        this.state = {
            country: '',
            state: '',
            city: ''
        }
    }

    render() {
        return (
            <>
                <SafeAreaView>
                    <StatusBar barStyle="dark-content" />

                    <View style={{ width: '100%', marginTop: 50, alignItems: 'center' }}>
                        <ThemedTextInput
                            onEndEditing={
                                (event) => {
                                    this.setState({
                                        country: event.nativeEvent.text.trim()
                                    })
                                }
                            }
                            underlined
                            placeHolder='Country' />

                        <ThemedTextInput
                            onEndEditing={
                                (event) => {
                                    this.setState({
                                        state: event.nativeEvent.text.trim()
                                    })
                                }
                            }
                            underlined
                            placeHolder='State' />

                        <ThemedTextInput
                            onEndEditing={
                                (event) => {
                                    this.setState({
                                        city: event.nativeEvent.text.trim()
                                    })
                                }
                            }
                            underlined
                            placeHolder='City' />
                    </View>

                    <View style={{
                        marginTop: 50
                    }}>
                        <ThemedButton
                            label="Submit"
                            onPress={this.submitAction}
                            disabled={false}
                        />
                    </View>
                    <Toast 
                    style={{backgroundColor: 'red'}}
                    ref="toast" position='center'/>
                </SafeAreaView>
            </>
        );
    }

    submitAction = () => {
        Keyboard.dismiss()

        const { country, state, city} = this.state;

        let errorMessage = ''
        if (country.length === 0) {
            errorMessage = 'Please enter Country name'
        }
        else if (state.length === 0) {
            errorMessage = 'Please enter State name'
        }
        else if (city.length === 0) {
            errorMessage = 'Please enter City name'
        }
        
        if (errorMessage.length) {
            this.refs.toast.show(errorMessage, 500, () => {})
        }
        else {
            this.props.navigation.navigate('weatherDashboard')
        }
    }
}