import React, { Component } from 'react';
import {
    SafeAreaView,
    View,
    StyleSheet,
    Text
} from 'react-native';

import Header from 'molecules/Header';
import menuIcon from '../../assets/Images/menu.png';
import { connect } from 'react-redux';
import { fetchWeatherData } from '../../utils/apiHandler';
import LinearGradient from 'react-native-linear-gradient';

class weatherDashboard extends Component {

    componentDidMount() {
        this.props.fetchWeatherData()
    }

    render() {
        const { weatherData } = this.props;
        return (
            <View style={styles.container}>
                <SafeAreaView>
                    <Header
                        title={weatherData ? `${weatherData.name} Weather forcast` : ' '}
                        leftIcon={menuIcon}
                        onLeftPress={() => this.props.navigation.navigate('viewProfile')} />

                    {
                        weatherData &&
                        <View style={styles.container}>
                            <LinearGradient colors={['#FB7BA2', '#FCE043']} style={styles.topContainer}>
                                <Text style={styles.temp}>
                                    {weatherData.main.temp} °F
                                </Text>
                            </LinearGradient>
                            <LinearGradient colors={['#647DEE', '#7F53AC']} style={[styles.bottomContainer]}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.property}>
                                        Wind Speed     :    
                                </Text>

                                    <Text style={styles.value}>
                                        {weatherData.wind.speed} /kmph
                                </Text>
                                </View>

                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.property}>
                                        Pressure       :
                                </Text>

                                    <Text style={styles.value}>
                                        {weatherData.main.pressure} SI
                                </Text>
                                </View>

                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.property}>
                                        Humidity        :
                                </Text>

                                    <Text style={styles.value}>
                                        {weatherData.main.humidity} g/m3
                                </Text>
                                </View>

                            </LinearGradient>
                        </View>
                    }
                </SafeAreaView>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    weatherData: state.weatherReport.weatherData,
});

const mapDispatchToProps = dispatch => ({
    fetchWeatherData: () => dispatch(fetchWeatherData())
})

const styles = StyleSheet.create({
    topContainer: {
        width: '100%',
        height: 200,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bottomContainer: {
        width: '100%',
        height: 400,
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    container: {
        flex: 1,
    },
    temp: {
        color: 'white',
        fontSize: 35,
        alignSelf: 'center',
        fontWeight: '500'
    },
    property: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
        fontWeight: '500'
    },
    value: {
        color: 'white',
        fontSize: 18,
        alignSelf: 'center',
        fontWeight: '300'
    }

})

export default connect(mapStateToProps, mapDispatchToProps)(weatherDashboard);