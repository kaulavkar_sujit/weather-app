import React from 'react';
import PropTypes from 'prop-types';
import {
  View, ViewPropTypes,
} from 'react-native';

const Divider = ({
  color,
  marginHorizontal,
  marginVertical,
  marginTop,
  marginBottom,
  style,
  ...props
}) => (
  <View
    {...props}
    style={[{
      height: 1,
      backgroundColor: color,
      marginHorizontal,
      marginVertical,
      marginTop,
      marginBottom,
    }, style]}
  />
);

Divider.propTypes = {
  marginHorizontal: PropTypes.number,
  marginVertical: PropTypes.number,
  marginTop: PropTypes.number,
  marginBottom: PropTypes.number,
  color: PropTypes.string,
  style: ViewPropTypes.style,
};

Divider.defaultProps = {
  marginHorizontal: 0,
  marginVertical: 0,
  marginTop: 0,
  marginBottom: 0,
  color: '#E3E3E3',
  style: {},
};

export default Divider;
