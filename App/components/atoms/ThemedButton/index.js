import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ViewPropTypes,
} from 'react-native';

import ThemedText from 'atoms/ThemedText';

const ThemedButton = (props) => {
  const {
    containerStyle,
    textStyle,
    label,
    onPress,
    disabled,
    backgroundColor,
    ...others
  } = props;

  return (
    <TouchableOpacity
      style={[
        styles.buttonContainer,
        containerStyle,
        { backgroundColor: backgroundColor },
      ]}
      disabled={disabled}
      onPress={onPress}
      {...others}>

      <ThemedText
        style={[
          styles.buttonText,
          textStyle,
        ]}
      >
        {label}
      </ThemedText>
    </TouchableOpacity>
  );
};

ThemedButton.propTypes = {
  textStyle: Text.propTypes.style,
  containerStyle: ViewPropTypes.style,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  onPress: PropTypes.func,
};

ThemedButton.defaultProps = {
  containerStyle: {},
  textStyle: {},
  label: '',
  disabled: false,
  onPress: () => { },
};

const styles = StyleSheet.create({
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: '90%',
    height: 50,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#CFCFCF',
  },
  buttonText: {
    textAlign: 'center',
  },
});

export default ThemedButton;
