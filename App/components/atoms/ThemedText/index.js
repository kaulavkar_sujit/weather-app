import React from 'react';
import PropTypes from 'prop-types';

import {
  Text,
  StyleSheet,
} from 'react-native';

const ThemedText = (props) => {
  const {
    style,
    ...others
  } = props;  
  return (
    <Text
      style={[
        styles.text,
        style,
      ]}
      {...others}
    />
  );
};

ThemedText.propTypes = {
  style: Text.propTypes.style, 
};

ThemedText.defaultProps = {
  style: {},
};

const styles = StyleSheet.create({
  text: {
    textAlign: 'left',
    color: 'black',    
  },
});

export default ThemedText;
