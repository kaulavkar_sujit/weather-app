import React from 'react';
import PropTypes from 'prop-types';
import {
  ViewPropTypes,
  View,
  ActivityIndicator
} from 'react-native';

const Spinner = ({
  color,
  style,
  size,
  ...props
}) => {
  return (
    <View
      style={[
        {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        },
        style,
      ]}
      {...props}
    >
            <ActivityIndicator size={size} color={color} />
    </View>

  );
};

Spinner.propTypes = {
  color: PropTypes.string,
  style: ViewPropTypes.style,
  size: PropTypes.oneOf(['large', 'small']),
};

Spinner.defaultProps = {
  color: 'gray',
  style: {},
  size: 'small',
};

export default Spinner;
