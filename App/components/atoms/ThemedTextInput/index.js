import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TextInput,
  Dimensions,
  PixelRatio,
} from 'react-native';
const obj = Dimensions.get('window');
const pr = PixelRatio.get();
const actualHeight = obj.height * pr;

const ThemedTextInput = (props) => {
  const {
    keyboardType,
    underlined,
    isPassword,
    maxLength,
    style,
    placeHolder,
    autoCapitalize,
    inputRef,
    containerStyle,
    placeholderTextColor,
    onEndEditing,
    onSubmitEditing,
    ...others
  } = props;
  return (
    <View style={[
      styles.containerStyle,
      containerStyle
    ]}>
      <TextInput
        style={[
          styles.textInputContainer,
          underlined && styles.underlined,
          style,
        ]}
        placeholderTextColor={placeholderTextColor}
        placeholder={placeHolder}
        keyboardType={keyboardType}
        secureTextEntry={isPassword}
        underlineColorAndroid={'transparent'}
        autoCapitalize={autoCapitalize}
        maxLength={maxLength || Number.MAX_SAFE_INTEGER}
        ref={inputRef}
        onEndEditing={onEndEditing}
        onSubmitEditing={onSubmitEditing}
        {...others}
      />
    </View>
  );
};

ThemedTextInput.propTypes = {
  keyboardType: PropTypes.string,
  isPassword: PropTypes.bool,
  underlined: PropTypes.bool,
  style: TextInput.propTypes.style,
  maxLength: PropTypes.number,
  autoCapitalize: PropTypes.string,
  inputRef: PropTypes.func,
  placeHolder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
};

ThemedTextInput.defaultProps = {
  keyboardType: 'default',
  isPassword: false,
  underlined: false,
  maxLength: 0,
  style: {},
  autoCapitalize: 'none',
  inputRef: () => { },
  placeHolder: '',
  placeholderTextColor: '#D2D2D2'
};

const styles = StyleSheet.create({
  containerStyle: {
    width: '90%',
    height: 80,
  },
  textInputContainer: {
    paddingVertical: actualHeight >= 1080 ? 10 : 4,
    paddingHorizontal: 10,
  },
  underlined: {
    borderColor: '#CFCFCF',
    borderWidth: 1,
    borderRadius: 5,
  },
});

export default ThemedTextInput;
