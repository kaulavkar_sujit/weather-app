import React from 'react';
import { PropTypes } from 'prop-types';
import ImagePicker from 'react-native-image-picker';
import { compose, withHandlers, withState, branch, renderComponent } from 'recompose';
import {
  View,
  StyleSheet,
  ViewPropTypes,
  Image,
  Text,
} from 'react-native';

import {
  ThemedText,
} from 'atoms';

import {
  ImageWithDefault,
} from 'molecules';

const enhance = compose(
  withState('uri', 'setUri', null),
  withState('error', 'setError', null),
  withHandlers({
    chooseImage: ({ setUri, setError, onImageUpload, onButtonClick, onCancel }) => () => {
      if (onButtonClick) {
        onButtonClick();
      }
      const options = {
        title: 'Select Avatar',
      };
      ImagePicker.showImagePicker(options, (response) => {
        if (response.error) {
          setError(response.error);
        } else if (!response.didCancel) {
          setUri(response.uri);
          onImageUpload(response.uri); // callback for caller to get the uri
        } else {
          if (onCancel) {
            onCancel();
          }
        }
      });
    },
  }),
);

const ImageUploader = (props) => {
  const {
    containerStyle,
    imageStyle,
    textStyle,
    uri,
    chooseImage,
    text,
    initialImage,
    error,
    ...others
  } = props;
  return (
    <View style={styles.container}>
      <ImageWithDefault
        containerStyle={containerStyle}
        imageStyle={imageStyle}
        uri={uri || initialImage}
        icon={'upload'}
        hideRing
        onPress={chooseImage}
        {...others}
      />
      {text ?
        <ThemedText
          style={[styles.text, textStyle]}
          type={'subHeader'}
          color={'#FF9052'}
          onPress={chooseImage}
        >
          {text}
        </ThemedText>
      : null}
      {
        !!error && <ErrorCompoent error={error} />
      }
    </View>
  );
};

ImageUploader.propTypes = {
  containerStyle: ViewPropTypes.style,
  imageStyle: Image.propTypes.style,
  textStyle: Text.propTypes.style,
  uri: PropTypes.string,
  text: PropTypes.string,
  initialImage: PropTypes.string,
  chooseImage: PropTypes.func,
};

ImageUploader.defaultProps = {
  containerStyle: {},
  imageStyle: {},
  textStyle: {},
  uri: null,
  text: '',
  initialImage: null,
  chooseImage: () => {},
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // flexDirection: 'column',
  },
  text: {
    lineHeight: 22,
    paddingTop: 16,
    marginTop: 40,
  },
});

export default enhance(ImageUploader);
