import React from 'react';
import PropTypes from 'prop-types';

import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

const HEADER_HEIGHT = 44;

const Header = (props) => {
  const {
    leftIcon,
    title,
    onLeftPress,
    containerStyle
  } = props;
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={styles.leftContainer}>
        {
          leftIcon && <TouchableOpacity
            style={styles.leftButton}
            onPress={onLeftPress}>
            <Image resizeMode='contain' style={styles.leftImage} source={leftIcon} />
          </TouchableOpacity>
        }
      </View>

      <View style={styles.titleContainer}>
        {
          title && <Text style={styles.title}>{title}</Text>
        }
      </View>
    </View>
  );
};

Header.propTypes = {
  showLeft: PropTypes.bool,
  showRight: PropTypes.bool,
  leftIcon: PropTypes.string,
  title: PropTypes.string,
  rightIcon: PropTypes.string,
  leftText: PropTypes.string,
  rightText: PropTypes.string,
  onLeftPress: PropTypes.func,
  onRightPress: PropTypes.func,
  containerStyle: Text.propTypes.style,
};

Header.defaultProps = {
  showLeft: true,
  showRight: true,
  leftIcon: null,
  title: null,
  rightIcon: null,
  leftText: null,
  rightText: null,
  onLeftPress: () => { },
  onRightPress: () => { },
  containerStyle: {},
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    zIndex: 999,
    elevation: 0,
    height: HEADER_HEIGHT,
    borderBottomWidth: 1,
    borderBottomColor: '#E7E7E7',
    flexDirection: 'row'
  },
  leftContainer: {
    height: '100%',
    width: 50,
  },
  leftButton: { height: '100%', width: 50, justifyContent: 'center', alignItems: 'center' },
  leftImage: { height: 25, width: 25 },
  titleContainer: {
    alignSelf: 'center',    
  },
  title: {
    fontWeight: '500',
    fontSize: 18
  }
});

export default Header;