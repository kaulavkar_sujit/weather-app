## Atomic Design
`components` folder follows [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/) principle.

To enforce maximum reusability of our atomic components, some rules are applied to them.
1. Atomic components should have minimal external dependency.
2. Atomic components should not be bound to app state management (redux)
