
import axios from 'axios';
import { weatherDataSuccess } from '../store/actions';

export const fetchWeatherData = () => {
    return dispatch => {
            axios.get('https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22').then((result) => {                                
            dispatch(weatherDataSuccess(result.data))
          })
    }
}
